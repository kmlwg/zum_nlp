# Projekt zaliczeniowy ZUM nlp

## Dane
- Około 92 tys. tweetów w języku angielski dot. COVID19
- Zebrane w między lipcem a sierpniem 2020
- Dodany sentyment przy pomocy VADERA, 0 - negatywny, 1 - neutralny/pozytywny

## Zastosowane metody
- Naiwny Bayes
- Regresja logistyczna 
- SVM
- Sieci konwulucyjne
- Bert
